/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gymapp.repositories;

import java.io.Serializable;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author andreiiorga
 */
public abstract class AbstractHibernateDAO <T extends Serializable>{
     private Class<T> clazz;
    private Session session = HibernateUtil.getSessionFactory().openSession();

    public void setClazz(final Class<T> clazzToSet) {
        clazz = clazzToSet;
    }


    public T findOne(final int id) {
        T object = (T) session.get(clazz, id);
        session.update(object);
        session.evict(object);
        return object;
    }


    public List<T> findAll() {
        return session.createQuery("from " + clazz.getName()).list();
    }


    public void save(final T entity) {
        session.beginTransaction();
        session.persist(entity);
        session.getTransaction().commit();
    }


    public void update(final T entity) {
        session.beginTransaction();
        session.update(entity);
        session.getTransaction().commit();
    }


    public void delete(final T entity) {
        session.beginTransaction();
        session.delete(entity);
        session.getTransaction().commit();
    }


    public void deleteById(final int id) {
        final T entity = findOne(id);
        session.beginTransaction();
        delete(entity);
        session.getTransaction().commit();
    }

    protected final Session getCurrentSession() {
        return session;
    }
}
