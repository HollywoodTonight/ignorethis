package com.mycompany.gymapp;

import com.mycompany.gymapp.models.Customer;
import com.mycompany.gymapp.models.Type;
import com.mycompany.gymapp.repositories.CustomerRepository;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class FileChooserSample extends Application {

    private TableView<Customer> table = new TableView<Customer>();

    final HBox hb = new HBox();

    //CustomerRepository customerRepository = new CustomerRepository();
    private List<Customer> customers;
    private ObservableList<Customer> customerss;

    public FileChooserSample() {
        /*for (int i = 0; i < 10; i++) {
            CustomerRepository customerRepository = new CustomerRepository();
            customerRepository.save(new Customer("Iorga", "Andrei", 24 + i, Type.Student, Date.valueOf(LocalDate.now()), Date.valueOf(LocalDate.now()), i));
        }
        System.out.println("Salvat!");

        customers = customerRepository.findAll();
        customerss = FXCollections.observableArrayList(customers);*/
    }

    private final ObservableList<Customer> data
            = FXCollections.observableArrayList(
                    new Customer("Jacob", "Smith", 23, Type.Normal, Date.valueOf(LocalDate.now()), Date.valueOf(LocalDate.now()), 0),
                    new Customer("Isabella", "Johnson", 24, Type.Student, Date.valueOf(LocalDate.now()), Date.valueOf(LocalDate.now()), 1),
                    new Customer("Ethan", "Williams", 25, Type.Normal, Date.valueOf(LocalDate.now()), Date.valueOf(LocalDate.now()), 2),
                    new Customer("Emma", "Jones", 26, Type.Student, Date.valueOf(LocalDate.now()), Date.valueOf(LocalDate.now()), 3),
                    new Customer("Michael", "Brown", 27, Type.Normal, Date.valueOf(LocalDate.now()), Date.valueOf(LocalDate.now()), 4));

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        Scene scene = new Scene(new Group());
        stage.setTitle("Table View Sample");
        stage.setWidth(800);
        stage.setHeight(550);
        final Label label = new Label("Gym Membership");
        label.setFont(new Font("Arial", 20));
        table.setEditable(true);

        //----------------------------------------------
        TableColumn firstNameCol = new TableColumn("First Name");
        firstNameCol.setCellValueFactory(
                new PropertyValueFactory<Customer, String>("firstName"));

        firstNameCol.setCellFactory(column -> {
            return new TableCell<Customer, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        setText(item);

                        Customer c1 = getTableView().getItems().get(getIndex());
                        if (c1.IsActive()) {
                            setTextFill(Color.CHOCOLATE);
                        }
                    }
                }
            };
        });
        /*
        firstNameCol.setCellFactory(TextFieldTableCell.forTableColumn());
        firstNameCol.setOnEditCommit(
                new EventHandler<CellEditEvent<Customer, String>>() {
            @Override
            public void handle(CellEditEvent<Customer, String> t) {
                ((Customer) t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).setFirstName(t.getNewValue());
            }
        }
        );
         */

        //----------------------------------------------
        // Creating the columns of the table and
        // associating them with the Property Value
        // of the Person class
        TableColumn lastNameCol = new TableColumn("Last Name");
        lastNameCol.setMinWidth(100);
        lastNameCol.setCellValueFactory(
                new PropertyValueFactory<Customer, String>("lastName"));

        lastNameCol.setCellFactory(column -> {
            return new TableCell<Customer, String>() {
                @Override
                protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);

                    if (item == null || empty) {
                        setText(null);
                        setStyle("");
                    } else {
                        setText(item);
                        Customer c1 = getTableView().getItems().get(getIndex());
                        if (c1.IsActive()) {
                            setTextFill(Color.CHOCOLATE);
                        }
                    }
                }
            };
        });

        TableColumn ageCol = new TableColumn("Age");
        ageCol.setMinWidth(50);
        ageCol.setCellValueFactory(
                new PropertyValueFactory<Customer, String>("age"));

        TableColumn startSubscriptionCol = new TableColumn("Start Subscription");
        startSubscriptionCol.setMinWidth(10);
        startSubscriptionCol.setMaxWidth(80);
        startSubscriptionCol.setCellValueFactory(
                new PropertyValueFactory<Customer, Date>("startSubscription"));

        TableColumn customerTypeCol = new TableColumn("Type");
        customerTypeCol.setMinWidth(10);
        customerTypeCol.setMaxWidth(50);
        customerTypeCol.setCellValueFactory(
                new PropertyValueFactory<Customer, String>("type"));

        TableColumn emailCol = new TableColumn("Email");
        emailCol.setMinWidth(200);
        emailCol.setCellValueFactory(
                new PropertyValueFactory<Customer, String>("email"));
        // ------------------------------------------
        table.setItems(data); //customerss
        table.getColumns().addAll(firstNameCol, lastNameCol, ageCol,
                startSubscriptionCol,
                customerTypeCol, emailCol);
        // -------------------------------------------

        final TextField addFirstName = new TextField();
        addFirstName.setPromptText("First Name");
        addFirstName.setMaxWidth(firstNameCol.getPrefWidth());

        final TextField addLastName = new TextField();
        addLastName.setMaxWidth(lastNameCol.getPrefWidth());
        addLastName.setPromptText("Last Name");

        final TextField addAge = new TextField();
        addAge.setMaxWidth(ageCol.getPrefWidth());
        addAge.setPromptText("Age");

        final TextField addType = new TextField();
        addType.setMaxWidth(customerTypeCol.getPrefWidth());
        addType.setPromptText("Type");

        final TextField addEmail = new TextField();
        addEmail.setMaxWidth(emailCol.getPrefWidth());
        addEmail.setPromptText("Email");
        
        final CheckBox activateSubscription = new CheckBox("Activate");
        activateSubscription.setMaxWidth(ageCol.getPrefWidth());

        final Button addButton = new Button("Add");
        addButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                // TO DO: Checking for proper inserts
                if (addFirstName.getText().equals("")) {
                    System.out.println("Must enter a name!");
                } else {

                    Customer c1 = new Customer(addLastName.getText(), addFirstName.getText(), Integer.parseInt(addAge.getText()),
                            addType.getText().equals("Normal") ? Type.Normal : Type.Student, Date.valueOf(LocalDate.now()), Date.valueOf(LocalDate.now()), 1);
                    boolean isActive = activateSubscription.isSelected();
                    c1.setIsActive(isActive);
                    data.add(c1);

                    //customerRepository.save(c1);
                    System.out.println("Saved");

                    addFirstName.clear();
                    addLastName.clear();
                    addAge.clear();
                    addType.clear();
                    addEmail.clear();
                    activateSubscription.setSelected(false);
                }
            }
        });

        final Button refreshButton = new Button("Refresh");
        refreshButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                System.out.println("Refresh Pressed!");
                //customers = customerRepository.findAll();
                //customerss = FXCollections.observableArrayList(customers);
            }
        });

        hb.getChildren().addAll(addFirstName, addLastName, addAge, addType, addEmail,
                                activateSubscription, addButton, refreshButton);
        hb.setSpacing(3);

        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(label, table, hb);
        ((Group) scene.getRoot()).getChildren().addAll(vbox);
        stage.setScene(scene);
        stage.show();
    }
}
