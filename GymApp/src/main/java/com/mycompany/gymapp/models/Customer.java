/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gymapp.models;

/**
 *
 * @author alexandru.tudoran
 */
import java.io.Serializable;
import java.util.Date;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private Date onCreate;
    private Date startSubscription;
    private String firstName;
    private String lastName;
    private int age;
    private Type type;
    private boolean isActive = true;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getType() {
        if (type == Type.Normal) {
            return "Normal";
        } else {
            return "Student";
        }
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setUniqueID(int uniqueID) {
        this.uniqueID = uniqueID;
    }
    private int uniqueID;

    public Date getOnCreate() {
        return onCreate;
    }

    public void setOnCreate(Date onCreate) {
        this.onCreate = onCreate;
    }

    public Date getStartSubscription() {
        return startSubscription;
    }

    public void setStartSubscription(Date startSubscription) {
        this.startSubscription = startSubscription;
    }

    public int getUniqueID() {
        return uniqueID;
    }

    public Customer(String pLastName, String pFirstName, int pAge, Type pType, Date pOnCreate, Date pStartSubscription, int pID) {

        this.firstName = pFirstName;
        this.lastName = pLastName;
        this.age = pAge;
        this.type = pType;
        this.onCreate = pOnCreate;
        this.startSubscription = pStartSubscription;
        this.uniqueID = pID;

    }

    public Customer(int ID) {
        this.uniqueID = ID;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Customer() {

    }

    public boolean IsActive() {

        if ((startSubscription.getTime() - System.currentTimeMillis()) > 846000000) {
            isActive = false;
        }
        return isActive;
    }

    public void ActivateSubscription() {
        isActive = true;
    }
    
    public void InactivateSubscription(){
        isActive = false;
    }
}
